package ir.driq.phonebook.application.addoreditcontact.dependencyinjection;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;

import dagger.Module;
import dagger.Provides;
import ir.driq.phonebook.application.addoreditcontact.AddOrEditContactViewModel;

/**
 * Module of AddOrEditContactViewModel
 */

@Module
public class AddOrEditContactModule {

    private Fragment fragment;

    public AddOrEditContactModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    AddOrEditContactViewModel provideAddOrEditContactViewModel () {
        return ViewModelProviders.of(fragment).get(AddOrEditContactViewModel.class);
    }
}
