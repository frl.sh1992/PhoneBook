package ir.driq.phonebook.application.contacts.dependencyinjection;

import android.arch.lifecycle.ViewModelProviders;
import android.support.v4.app.Fragment;

import dagger.Provides;
import ir.driq.phonebook.application.contacts.ContactsViewModel;

/**
 * Module of contacts dependency injection
 */

@dagger.Module
public class ContactsModule {

    private Fragment fragment;

    public ContactsModule(Fragment fragment) {
        this.fragment = fragment;
    }

    @Provides
    ContactsViewModel provideViewModel(Fragment fragment) {
        return ViewModelProviders.of(fragment).get(ContactsViewModel.class);
    }

    @Provides
    Fragment provideFragment() {
        return fragment;
    }
}
