package ir.driq.phonebook.application.addoreditcontact.dependencyinjection;

import dagger.Component;
import ir.driq.phonebook.application.addoreditcontact.AddOrEditContactFragment;
import ir.driq.phonebook.application.addoreditcontact.AddOrEditContactViewModel;

/**
 * Component for dependency injection in AddOrEditContactViewModel
 */

@Component(modules = {AddOrEditContactModule.class})
public interface AddOrEditContactComponent {

    void inject(AddOrEditContactFragment fragment);

    AddOrEditContactViewModel getAddOrEditContactViewModel();
}
