package ir.driq.phonebook.application.contacts.dependencyinjection;


import ir.driq.phonebook.application.contacts.ContactsFragment;
import ir.driq.phonebook.application.contacts.ContactsViewModel;

/**
 * Component for dependency injection in contacts activity
 */

@dagger.Component(modules = {ContactsModule.class})
public interface ContactsComponent {

    void inject(ContactsFragment contactsFragment);

    ContactsViewModel getContactsViewModel();
}
