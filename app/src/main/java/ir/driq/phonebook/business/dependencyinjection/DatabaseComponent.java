package ir.driq.phonebook.business.dependencyinjection;

import javax.inject.Singleton;

import dagger.Component;
import ir.driq.phonebook.BaseApplication;
import ir.driq.phonebook.business.database.AppDatabase;

/**
 * Database Component for dependency injection
 */

@Singleton
@Component(modules = DatabaseModule.class)
public interface DatabaseComponent {

    void inject(BaseApplication baseApplication);

    AppDatabase getAppDatabase();
}
