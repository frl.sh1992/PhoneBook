package ir.driq.phonebook.business.dependencyinjection;

import android.arch.persistence.room.Room;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ir.driq.phonebook.business.database.AppDatabase;

/**
 * Database module for dependency injection
 */

@Module
public class DatabaseModule {

    private static final String DATABASE_NAME = "database.db";
    private Context context;

    public DatabaseModule(Context context) {
        this.context = context;
    }

    @Provides
    Context provideContext() {
        return context;
    }

    @Provides @Singleton
    AppDatabase provideAppDatabase() {
        return Room.databaseBuilder(context, AppDatabase.class, DATABASE_NAME).allowMainThreadQueries().build();
    }
}
