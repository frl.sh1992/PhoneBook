package ir.driq.phonebook;

import android.app.Application;

import javax.inject.Inject;

import ir.driq.phonebook.business.database.AppDatabase;
import ir.driq.phonebook.business.dependencyinjection.DaggerDatabaseComponent;
import ir.driq.phonebook.business.dependencyinjection.DatabaseModule;

/**
 * Base Application
 */

public class BaseApplication extends Application {

    @Inject
    AppDatabase appDatabase;

    @Override
    public void onCreate() {
        super.onCreate();
        appDatabase = DaggerDatabaseComponent.builder().databaseModule(new DatabaseModule(this)).build().getAppDatabase();
    }

    public AppDatabase getAppDatabase() {
        return appDatabase;
    }
}
